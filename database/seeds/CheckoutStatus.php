<?php

use Illuminate\Database\Seeder;

class CheckoutStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "Annulée"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "En attente de paiement à la livraison"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "En attente de réapprovisionnement (Non Payé)"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "En attente de réapprovisionnement (Payé)"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "En attente de virement bancaire"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "En attente de paiement par chèque"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "En cours de préparation"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "Erreur de paiement"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "Expédié"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "Livré"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "Paiement à distance accepté"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "Paiement Accepté"]);
        \Illuminate\Support\Facades\DB::table('checkout_statuses')->insert(["name"  => "Remboursé"]);
    }
}
