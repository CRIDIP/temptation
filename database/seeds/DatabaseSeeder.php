<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            "name"  => "Mockelyn Maxime",
            "email" => "mmockelyn@gmail.com",
            "password" => bcrypt('1992_Maxime'),
            "group" => 1
        ]);
    }
}
