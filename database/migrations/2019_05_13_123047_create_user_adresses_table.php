<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAdressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_adresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("user_id")->unsigned();
            $table->string("nameAddress");
            $table->string("nom");
            $table->string("prenom");
            $table->string("societe")->nullable();
            $table->string("numTVA")->nullable();
            $table->string("adresse");
            $table->string("c_adresse")->nullable();
            $table->string("codePostal", 5);
            $table->string('ville');
            $table->string('pays');
            $table->string('telephone')->nullable();
            $table->text("other")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_adresses');
    }
}
