<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string("numAccount");
            $table->integer("civilite")->comment("0: Monsieur |1: Madame |2: Mademoiselle");
            $table->dateTime("dateNaissance");
            $table->integer('newsletter')->default(0)->comment("0: Non Inscrit |1: Inscrit");
            $table->integer('optin')->default(0)->comment("0: Non Inscrit |1: Inscrit");
            $table->integer("state")->default(1)->comment("0: Désactivé |1: Activé");
            $table->string("langue")->default('fr');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
