<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_shippings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('checkout_id');
            $table->integer('shipping_id');
            $table->string('poids')->default(0);
            $table->string('ship_amount')->default(0);
            $table->string('numSuivie');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_shippings');
    }
}
