<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('numOrder');
            $table->dateTime('dateOrder');
            $table->string('totalOrder');
            $table->integer('stateOrder')->default(0)->comment(
                "
                0: Commande en cours |
                1: Annulée |
                2: En attente de paiement à la livraison |
                3: En attente de réapprovisionnement (Non payé) |
                4: En attente de réapprovisionnement (Payé) |
                5: En attente de virement bancaire |
                6: En cours de préparation |
                7: Erreur de Paiement |
                8: Expédié |
                9: Livré |
                10: Paiement Accepté |
                11: Remboursé
                "
            );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
