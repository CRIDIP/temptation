@extends("layouts.app")

@section("css")
    <link href="/assets/css/chosen.css" rel="stylesheet">
    <style>
        /**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
        .StripeElement {
            box-sizing: border-box;

            height: 40px;

            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
@endsection

@section("content")
    <section class="section-checkout">
        <div class="container">
            <div class="row-fluid">
                <div class="form-holder">
                    <div class="span12">
                        <h1 class="title">Passage de commande</h1>
                        @if (Session::has('success'))
                            <div class="alert alert-success text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <h2 style="font-weight: bold;">Paiement Effectuer</h2>
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert alert-danger text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <h2 style="font-weight: bold;">Erreur</h2>
                                <p>{!! Session::get('error') !!}</p>
                            </div>
                        @endif
                        <form action="{{ route('Checkout.checkout') }}" method="POST"
                              class="require-validation"
                              data-cc-on-file="false"
                              data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                              id="payment-form">
                            @csrf
                            <input type="hidden" name="total" value="{{ $cart->total_cart+9.90 }}">
                            <input type="hidden" name="num_panier" value="{{ $cart->token }}">
                            <input type="hidden" name="cart" value="{{ $cart }}">
                            <div class="control-group">
                                <div class="controls">
                                    <h3><u>Identification</u></h3>
                                    <br>
                                    <div class="form-label ">Nom & Prénom</div>
                                    <input type="text" id="name"   name="name"  class="required span12 cusmo-input" value="{{ auth()->user()->name }}"  required/>
                                    <div class="form-label ">Adresse Mail</div>
                                    <input type="text" id="email"   name="email"  class="required span12 cusmo-input" value="{{ auth()->user()->email }}"  required/>
                                    <div class="form-label ">Numéro de Téléphone</div>
                                    <input type="text" id="telephone"   name="telephone"  class="required span12 cusmo-input" value="{{ auth()->user()->adresse->telephone }}" required/>
                                    <hr />
                                    <h3><u>Livraison & Facturation</u></h3>
                                    <br>
                                    <div class="form-label ">Adresse Postal</div>
                                    <input type="text" id="adresse"   name="adresse" value="{{ auth()->user()->adresse->adresse }}"  class="required span12 cusmo-input"  required/>
                                </div>
                                <div class="controls">
                                    <div class="form-label ">Code Postal</div>
                                    <input type="text" id="code_postal"   name="code_postal"  class="required span4 cusmo-input" value="{{ auth()->user()->adresse->codePostal }}"  required/>
                                    <div class="form-label ">Ville</div>
                                    <input type="text" id="ville"   name="ville"  class="required span12 cusmo-input" value="{{ auth()->user()->adresse->ville }}" required/>
                                </div>
                                <hr />
                                <div class="controls">
                                    <h3><u>Paiement de la commande</u></h3>
                                    <br>
                                    <table class="table">
                                        <tr>
                                            <td>Panier N°</td>
                                            <td>{{ $cart->token }}</td>
                                        </tr>
                                        <tr>
                                            <td>Sous-Total</td>
                                            <td>{{ formatCurrency($cart->total_cart) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total du transport</td>
                                            <td>{{ formatCurrency('9.90') }}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; font-size: 20px; background-color: #0d1c29; color: white">Total de la commande</td>
                                            <td style="font-weight: bold; font-size: 20px; background-color: #0d1c29; color: white">{{ formatCurrency($cart->total_cart+9.90) }}</td>
                                        </tr>
                                    </table>
                                    <br>
                                    <div class="controls">
                                        <div id="card-element">
                                            <!-- A Stripe Element will be inserted here. -->
                                        </div>
                                        <div id="card-errors" role="alert"></div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="cusmo-btn add-button text-md-center">Passer Commande</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="/assets/js/bootstrap-slider.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.raty.min.js"></script>
    <script src="/assets/js/jquery.icheck.min.js"></script>
    <script type="text/javascript" src="/assets/js/chosen.jquery.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script type="text/javascript">
        (function ($) {
// Create a Stripe client.
            var stripe = Stripe('{{ env("STRIPE_KEY") }}');
            let handler = StripeCheckout.configure({
                key: '{{ env("STRIPE_KEY") }}',
                locale: 'fr'
            });

// Create an instance of Elements.
            var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                    '::placeholder': {
                        color: '#aab7c4'
                    }
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

// Create an instance of the card Element.
            var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');

// Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
                var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
            });

// Handle form submission.
            var form = document.getElementById('payment-form');
            form.addEventListener('submit', function(event) {
                event.preventDefault();

                stripe.createToken(card).then(function(result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        // Send the token to your server.
                        stripeTokenHandler(result.token);
                    }
                });
            });

// Submit the form with the token ID.
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
            }
        })(jQuery)
    </script>
@endsection