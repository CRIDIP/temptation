@extends("layouts.app")

@section("css")

@endsection

@section("content")
    <section class="section-shopping-cart">
        <div class="container">
            <div class="row-fluid">

                <div class="span12">
                    <div class="page-content shopping-cart-page ">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <h2>Nous Contacter</h2>
                        <div class="form-holder">
                            <form action="{{ route('contactPost') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="form-label ">Nom & Prénom <span style="color: red;">*</span> </div>
                                        <input type="text"  name="name"  class="required span12 cusmo-input"  required/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="form-label ">Email <span style="color: red;">*</span></div>
                                        <input type="email"  name="email"  class="required span12 cusmo-input"  required/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="form-label ">Type de demande <span style="color: red;">*</span></div>
                                        <select class="required span12 cusmo-input" name="type" required>
                                            <option value="Information sur un produit">Information sur un produit</option>
                                            <option value="Conseil Commercial">Conseil commercial</option>
                                            <option value="Information sur votre facture">Information sur votre facture</option>
                                            <option value="Retour d'un produit">Retour d'un produit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="form-label ">N° de Facture</div>
                                        <input type="text"  name="fct_number"  class="required span12 cusmo-input"  />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="form-label ">Commentaire <span style="color: red;">*</span></div>
                                        <textarea class="required span12 cusmo-input" rows="5" name="commentaire" required></textarea>
                                    </div>
                                </div>
                                <button class="cusmo-btn narrow pull-right" type="submit" >Envoyer</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--<section class="section-homepage-subscribe">
        <div class="container">
            <div class="big-circle">

                get the
                <div class="big"><span>$</span>10</div>
                cupon

            </div>
            <div class="offer-text">
                Sign in for our newsletter and recieve a ten dollars cupon
            </div>
            <div class="email-holder">

                <div class="email-field">

                    <form>
                        <input class=" required email" name="email" data-placeholder="Enter here your email address..." />
                        <button class="newsletter-submit-btn" type="submit" value=""><i class="icon-plus"></i></button>

                    </form>

                </div>
            </div>
        </div>
    </section>-->
@endsection

@section("scripts")
    <script type="text/javascript" src="/assets/js/bootstrap-slider.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.raty.min.js"></script>
    <script type="text/javascript" src="/assets/js/chosen.jquery.min.js"></script>
    <script type="text/javascript">
        (function ($) {
            $("#cart").on("change", "#selectQuantity", function (e) {
                e.preventDefault()
                let table = $("#cart")
                let select = $(this);
                let product = select.attr('data-product')
                let cart = select.attr('data-cart')
                let price = $(".productTable").find('input[name="amount-price"]')

                $.ajax({
                    url: '/cart/updateProduct?product='+product+'&qte='+select.val()+'&cart='+cart,
                    method: "GET",
                    success: function (data) {
                        //console.log(data)
                        window.location.href='/cart'
                    }
                });

                console.log("table", table)
                console.log("select", select.val())
                console.log("select", product)
                console.log("price", price.val())
            })
            $("#refresh").on('click', function (e) {
                e.preventDefault()
                window.location.href='/cart'
            })
        })(jQuery)
    </script>
@endsection