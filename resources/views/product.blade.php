@extends("layouts.app")

@section("css")

@endsection

@section("content")
    <section class="section-two-columns">
        <div class="container">
            <div class="row-fluid">
                <div class="span12 ">
                    <div class="page-content">
                        <div class="products-page-head">
                            <h1>{{ $product->name }}</h1>
                            <div class="tag-line">
                                - {{ $product->recapitulatif }}
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span7">
                                <div class="flexslider product-gallery">
                                    <ul class="slides">
                                        <li data-thumb="/assets/images/product/{{ $product->id }}.jpg">
                                            <img alt=""  src="/assets/images/product/{{ $product->id }}.jpg" />
                                        </li>
                                        <!--<li data-thumb="images/p2.jpg">
                                            <img alt=""  src="images/p2.jpg" />
                                        </li>
                                        <li data-thumb="images/p3.jpg">
                                            <img alt="" src="images/p3.jpg" />
                                        </li>
                                        <li data-thumb="images/p4.jpg">
                                            <img alt="" src="images/p4.jpg" />
                                        </li>
                                        <li data-thumb="images/p5.jpg">
                                            <img alt="" src="images/p5.jpg" />
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="span5">
                                <div class="product-info-box">
                                    <!--<div class="star-holder">
                                        <strong>Rating</strong>
                                        <div class="star" data-score="3"></div>

                                        <div class="review-counter">
                                            2 reviews

                                        </div>
                                    </div>-->
                                    <hr>
                                    <div class="info-holder">
                                        <h4>Product ID: {{ $product->reference }}</h4>
                                        <p>
                                            {!! str_limit($product->description, 150, '...') !!}
                                        </p>
                                    </div>
                                    <hr>
                                    <!--<div class="drop-downs-holder">
                                        <div class="drop-selector capacity-selector">
                                            <span>Pick capacity</span>

                                            <select class="chosen-select">
                                                <option value="20ml">20 ml</option>
                                                <option value="30ml">30 ml</option>
                                                <option value="40ml">40 ml</option>
                                                <option value="50ml">50 ml</option>
                                                <option value="60ml">60 ml</option>
                                                <option value="70ml">70 ml</option>
                                                <option value="80ml">80 ml</option>
                                                <option value="90ml">90 ml</option>
                                                <option value="100ml">100 ml</option>
                                            </select>

                                        </div>

                                        <div class="drop-selector">
                                            <span>amount</span>
                                            <select class="chosen-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>

                                            </select>
                                        </div>
                                    </div>-->

                                    <hr>
                                    <div class="price-holder">
                                        <div class="price">
                                            <span>{{ formatCurrency($product->price) }}</span>
                                        </div>
                                    </div>
                                    <div class="buttons-holder" id="produit">
                                        <form id="addProduct" action="{{ route('Cart.addProduct') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <button type="submit" id="btnAddProduct" class="cusmo-btn add-button" >Ajouter au panier</button>
                                        </form>

                                        <form id="addProduct" action="{{ route('Cart.addWhitelist') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <a class="add-to-wishlist-btn"><i class="icon-plus"></i> Ajouter à ma liste</a>
                                        </form>
                                    </div>
                                </div>


                            </div>


                        </div>
                        <div class="product-tabs">
                            <div class="controls-holder nav-tabs">
                                <ul class="inline">
                                    <li class="active"><a data-toggle="tab" href="#description">Description</a></li>
                                    <li><a data-toggle="tab" href="#how-to-use">Comment l'utiliser</a></li>
                                    <li><a data-toggle="tab" href="#reviews">Avis (2)</a></li>
                                </ul>
                            </div>

                            <div class="tab-content">
                                <div id="description" class=" active tab-pane ">
                                    {!! $product->description !!}
                                </div>

                                <div id="how-to-use" class=" tab-pane ">
                                    {!! $product->howUse !!}
                                </div>

                                <!--<div id="reviews" class=" tab-pane ">
                                    <textarea placeholder="Your review here" class="span12" id="write-review-text"></textarea>

                                    <div class="review-info">
                                        <div class="remaining-chars">
                                            <span id="counter">210</span> characters left
                                        </div>

                                        <div class="star-holder">
                                            <strong>Rating</strong>
                                            <div class="star" data-score="3"></div>

                                            <button type="submit" class="cusmo-btn">add review</button>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="recent-reviews">
                                        <div class="review-item">
                                            <div class="row-fluid">
                                                <div class="span2">
                                                    <div class="thumb">
                                                        <img alt="avatar" src="images/default-avatar.png" />
                                                    </div>
                                                </div>
                                                <div class="span10">
                                                    <div class="body">
                                                        <h4>Angela</h4>
                                                        <span class="date">10.08.2013</span>
                                                        <p>
                                                            Pellentesque eleifend quam venenatis convallis consequat. Sed iaculis tortor eu ipsum fermentum, at commodo risus suscipit.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="review-item">
                                            <div class="row-fluid">
                                                <div class="span2">
                                                    <div class="thumb">
                                                        <img alt="avatar" src="images/default-avatar.png" />
                                                    </div>
                                                </div>
                                                <div class="span10">
                                                    <div class="body">
                                                        <h4>Kate</h4>
                                                        <span class="date">02.03.2013</span>
                                                        <p>
                                                            Pellentesque eleifend quam venenatis convallis consequat. Sed iaculis tortor eu ipsum fermentum, at commodo risus suscipit.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>
                                    </div>

                                </div>-->

                            </div>
                        </div>

                        <!--<div class="middle-header-holder">
                            <div class="middle-header">you will also like</div>
                        </div>
                        <div class="products-holder related-products">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="product-item">

                                        <img alt="" src="images/p1.jpg" />
                                        <h1>versace</h1>
                                        <div class="tag-line">
                                            <span>yellow diamond</span>
                                            <span>toilet water spray</span>
                                        </div>
                                        <div class="price">
                                            $270.00
                                        </div>
                                        <a class="cusmo-btn add-button" href="#">add to cart</a>

                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="product-item">
                                        <img alt="" src="images/p2.jpg" />
                                        <h1>estee lauder</h1>
                                        <div class="tag-line">
                                            <span>yellow diamond</span>
                                            <span>toilet water spray</span>
                                        </div>
                                        <div class="price">
                                            $122.00
                                        </div>
                                        <a class="cusmo-btn add-button" href="#">add to cart</a>
                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="product-item">
                                        <img alt="" src="images/p3.jpg" />
                                        <h1>burberry</h1>
                                        <div class="tag-line">
                                            <span>yellow diamond</span>
                                            <span>toilet water spray</span>
                                        </div>
                                        <div class="price">
                                            $120.00
                                        </div>
                                        <a class="cusmo-btn add-button" href="#">add to cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section("scripts")

@endsection()