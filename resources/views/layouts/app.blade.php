<!DOCTYPE html>
<html>
<head>

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title>{{ env('APP_NAME') }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='//fonts.googleapis.com/css?family=Raleway:400,500,700,600,800' rel='stylesheet' type='text/css'>
    <link href="/assets/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.css">

    <link href="/assets/css/flexslider.css" rel="stylesheet">



    <!--[if IE 7]>

    <link href="/assets/css/font-awesome/css/font-awesome-ie7.min.css" rel="stylesheet">

    <![endif]-->
    <link  rel="stylesheet" href="/assets/css/style.css">
    <link  rel="stylesheet" href="/assets/css/responsive.css">
    @yield("css")



</head>
<body class="homepage2">



<div class="wrapper">
    @include("include.header")

    @yield("content")

    @include("include.footer")

</div>


<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
<script src="/assets/css/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/assets/js/css_browser_selector.js"></script>

<script type="text/javascript" src="/assets/js/twitter-bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.easing-1.3.js"></script>

<script type="text/javascript" src="/assets/js/jquery.flexslider-min.js"></script>

<script type="text/javascript" src="/assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.0/dist/sweetalert2.all.js"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script type="text/javascript" src="/assets/js/script.js"></script>
<script type="text/javascript" src="/assets/js/custom.js"></script>
@yield("scripts")
</body>


</html>
