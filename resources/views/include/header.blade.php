<section class="section-head">
    <div class="container">
        <div class="row-fluid top-row">
            <div class="span4">
                <a class="logo" href="{{ route('home') }}">
                                <span class="icon">
                                    <img alt="" src="/assets/images/logo.png" />
                                </span>
                </a>




            </div>
            <div class="span8">


                <div class="top-menu cart-menu">
                    <ul class="inline">
                        @auth()
                            <li>
                                <a href="{{ route('construct') }}">Ma Liste de produit ( 0 ) </a>
                            </li>
                            <li>
                                <a href="{{ route('construct') }}">Mon Compte</a>
                            </li>
                            <li>
                                <a href="{{ route('Checkout.index') }}">Passer Commande</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}">Déconnexion</a>
                            </li>
                        @else
                            <li><a href="{{ route("login") }}">Connexion</a></li>
                            <li><a href="{{ route("login") }}">S'enregistrer</a></li>
                        @endauth

                        <li><a href="{{ route('contact') }}">Contact</a></li>

                        <li>
                            <div class="basket">
                                <?php
                                $cart = \App\Repository\Cart\CartRepository::getStaticCart();
                                ?>
                                <div class="basket-item-count">
                                    {{ \App\Repository\Cart\CartProductRepository::countStaticProduct($cart->id) }}
                                </div>
                                <div class="total-price-basket">
                                    {{ formatCurrency($cart->total_cart, true) }}
                                </div>
                                <div class="dropdown">
                                    <a class="dropdown-toggle" data-hover="dropdown" href="#">
                                        <img alt="basket" src="/assets/images/icon-basket.png" />
                                    </a>
                                    <ul class="dropdown-menu" >
                                        @foreach($cart->products as $product)
                                        <li>
                                            <div class="basket-item">
                                                <div class="row-fluid">
                                                    <div class="span4">
                                                        <div class="thumb">
                                                            <img alt="" src="/assets/images/product/{{ $product->product_id }}.jpg" />
                                                        </div>
                                                    </div>
                                                    <div class="span8">
                                                        <div class="title">{{ $product->product->name }}</div>
                                                        <div class="price">{{ formatCurrency($product->product->price) }}</div>
                                                    </div>
                                                </div>
                                                <a id="deleteProductCart" class="close-btn" href="{{ route('Cart.deleteProduct', $product->id) }}"></a>
                                            </div>
                                        </li>
                                        @endforeach

                                        <li class="checkout">
                                            <a href="{{ route('Cart.index') }}" class="cusmo-btn">Commander</a>
                                        </li>
                                    </ul>

                                </div>

                            </div>
                        </li>

                    </ul>


                </div>
            </div>
        </div>


    </div>

    <div class="top-categories">
        <div class="container">
            <div class="row-fluid">
                <div class="span12">
                    <ul class="inline top-cat-menu">
                        @foreach(\App\Repository\Product\ProductCategoryRepository::staticAll() as $category)
                        <li><a href="{{ route('categorie', $category->id) }}">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>

                    <select class="top-cat-menu dropdown">
                        @foreach(\App\Repository\Product\ProductCategoryRepository::staticAll() as $category)
                            <option value="{{ route('categorie', $category->id) }}">
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <!--<div class="span3">
                    <div class="search-field-holder">
                        <form>
                            <input class="span12" type="text" placeholder="Taper & appuyer sur Entrer">
                            <i class="icon-search"></i>
                        </form>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</section>