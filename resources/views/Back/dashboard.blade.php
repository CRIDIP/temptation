@extends("Back.Layout.app")

@section("style")

@endsection

@section("subheader")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Tableau de Bord</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Tableau de Bord </a>

                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <!--<a href="#" class="btn kt-subheader__btn-primary">
                        <i class="la la-angle-double-left"></i>
                        Actions &nbsp;
                    </a>-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Tableau de bord
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body kt-portlet__body--fluid">
                <div class="kt-widget12">
                    <div class="kt-widget12__content">
                        <div class="kt-widget12__item">
                            <div class="kt-widget12__info">
                                <span class="kt-widget12__desc">Total des commandes</span>
                                <span class="kt-widget12__value">
                                    @if(\App\Repository\Order\OrderRepository::staticCountOrder() != 0)
                                        {{ formatCurrency(\App\Repository\Order\OrderRepository::staticSumOrder()) }}
                                    @else
                                        0,00 €
                                    @endif
                                </span>
                            </div>
                            <div class="kt-widget12__info">
                                <span class="kt-widget12__desc">Date</span>
                                <span class="kt-widget12__value">
                                    @if(\App\Repository\Order\OrderRepository::staticCountOrder() != 0)
                                        {{ \App\Repository\Order\OrderRepository::getLastOrderDate() }}
                                    @else
                                        Néant
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="kt-widget12__item">
                            <div class="kt-widget12__info">
                                <span class="kt-widget12__desc">Revenue Moyen</span>
                                <span class="kt-widget12__value">
                                    @if(\App\Repository\Order\OrderRepository::staticCountOrder() != 0)
                                        {{ formatCurrency(\App\Repository\Order\OrderRepository::staticMoyOrder()) }}
                                    @else
                                        0,00 €
                                    @endif
                                </span>
                            </div>
                            <!--<div class="kt-widget12__info">
                                <span class="kt-widget12__desc">Revenue Margin</span>
                                <div class="kt-widget12__progress">
                                    <div class="progress kt-progress--sm">
                                        <div class="progress-bar kt-bg-brand" role="progressbar" style="width: 40%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="kt-widget12__stat">
																	40%
																</span>
                                </div>
                            </div>-->
                        </div>
                    </div>
                    <div class="kt-widget12__chart" style="height:250px;">
                        <canvas id="kt_chart_order_statistics"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-4">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-graph"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Aperçu de l'activité
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget1">
                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Nombre de visiteur en ligne</h3>
                                <span class="kt-widget1__desc">Dans les 15 dernières minutes</span>
                            </div>
                            <span class="kt-widget1__number kt-font-brand">34</span>
                        </div>

                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Paniers Actifs</h3>
                                <span class="kt-widget1__desc">Dans les 15 dernières minutes</span>
                            </div>
                            <span class="kt-widget1__number kt-font-danger">0</span>
                        </div>

                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Commande en attente</h3>
                            </div>
                            <span class="kt-widget1__number kt-font-danger">0</span>
                        </div>

                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Retour en attente</h3>
                            </div>
                            <span class="kt-widget1__number kt-font-danger">0</span>
                        </div>

                        <div class="kt-widget1__item">
                            <div class="kt-widget1__info">
                                <h3 class="kt-widget1__title">Produits en rupture de stock</h3>
                            </div>
                            <span class="kt-widget1__number kt-font-danger">5</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-graph"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Produits & Ventes
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#orderRecent" role="tab">
                                    Commandes Recentes
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="orderRecent">
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Les 10 dernières commandes
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Client</th>
                                                <th>Produits</th>
                                                <th>Total HT</th>
                                                <th>Date</th>
                                                <th>Etat</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $order)
                                                <tr>
                                                    <td>{{ $order->user->name }}</td>
                                                    <td>{{ \App\Repository\Order\OrderProductRepository::staticCountProductForOrder($order->id) }}</td>
                                                    <td>{{ formatCurrency($order->totalOrder) }}</td>
                                                    <td>{{ formatDate($order->dateOrder) }}</td>
                                                    <td>{!! \App\Repository\Order\OrderRepository::staticStateOrder($order->stateOrder, true, false) !!}</td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        function dashboard(){
            $.ajax({
                url: '/backoffice/sale/order/orderMonth',
                success: function (data) {
                    let container = KTUtil.getByID("kt_chart_order_statistics")
                    if(!container){return;}

                    let color = Chart.helpers.color
                    let barChartData = {
                        labels: data.labels,
                        datasets: [
                            {
                                fill: true,
                                backgroundColor: color(KTApp.getStateColor('brand')).alpha(0.6).rgbString(),
                                borderColor : color(KTApp.getStateColor('brand')).alpha(0).rgbString(),
                                pointHoverRadius: 4,
                                pointHoverBorderWidth: 12,
                                pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                                pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                                pointHoverBackgroundColor: KTApp.getStateColor('brand'),
                                pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),

                                data: data.table
                            }
                        ]
                    };

                    let ctx = container.getContext('2d')
                    let chart = new Chart(ctx, {
                        type: 'line',
                        data: barChartData,
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: false,
                            scales: {
                                xAxes: [{
                                    categoryPercentage: 0.35,
                                    barPercentage: 0.70,
                                    display: true,
                                    scaleLabel: {
                                        display: false,
                                        labelString: 'Month'
                                    },
                                    gridLines: false,
                                    ticks: {
                                        display: true,
                                        beginAtZero: true,
                                        fontColor: KTApp.getBaseColor('shape', 3),
                                        fontSize: 13,
                                        padding: 10
                                    }
                                }],
                                yAxes: [{
                                    categoryPercentage: 0.35,
                                    barPercentage: 0.70,
                                    display: true,
                                    scaleLabel: {
                                        display: false,
                                        labelString: 'Value'
                                    },
                                    gridLines: {
                                        color: KTApp.getBaseColor('shape', 2),
                                        drawBorder: false,
                                        offsetGridLines: false,
                                        drawTicks: false,
                                        borderDash: [3, 4],
                                        zeroLineWidth: 1,
                                        zeroLineColor: KTApp.getBaseColor('shape', 2),
                                        zeroLineBorderDash: [3, 4]
                                    },
                                    ticks: {
                                        max: 70,
                                        stepSize: 10,
                                        display: true,
                                        beginAtZero: true,
                                        fontColor: KTApp.getBaseColor('shape', 3),
                                        fontSize: 13,
                                        padding: 10
                                    }
                                }]
                            },
                            title: {
                                display: false
                            },
                            hover: {
                                mode: 'index'
                            },
                            tooltips: {
                                enabled: true,
                                intersect: false,
                                mode: 'nearest',
                                bodySpacing: 5,
                                yPadding: 10,
                                xPadding: 10,
                                caretPadding: 0,
                                displayColors: false,
                                backgroundColor: KTApp.getStateColor('brand'),
                                titleFontColor: '#ffffff',
                                cornerRadius: 4,
                                footerSpacing: 0,
                                titleSpacing: 0
                            },
                            layout: {
                                padding: {
                                    left: 0,
                                    right: 0,
                                    top: 5,
                                    bottom: 5
                                }
                            }
                        }
                    })
                }
            })

        }
        (function ($) {
            dashboard();
        })(jQuery)
    </script>
@endsection