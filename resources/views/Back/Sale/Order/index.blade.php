@extends("Back.Layout.app")

@section("style")

@endsection

@section("subheader")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Commandes</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Ventes </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Commandes </a>

                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <!--<a href="#" class="btn kt-subheader__btn-primary">
                        <i class="la la-angle-double-left"></i>
                        Actions &nbsp;
                    </a>-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-graph"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Liste des commandes
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped- table-bordered table-hover" id="orders">
                <thead>
                <tr>
                    <th>Numéro de commande</th>
                    <th>Client</th>
                    <th>Date de la commande</th>
                    <th>Montant</th>
                    <th>Etat</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->numOrder }}</td>
                        <td>{{ $order->user->name }}</td>
                        <td>{{ $order->dateOrder->format("d/m/Y") }}</td>
                        <td>{{ formatCurrency($order->totalOrder) }}</td>
                        <td align="center">{!! \App\Repository\Order\OrderRepository::staticStateOrder($order->stateOrder, true, false) !!}</td>
                        <td align="center">
                            <a href="{{ route('Back.Order.show', $order->id) }}" class="btn btn-brand btn-elevate btn-circle btn-icon" data-toggle="kt-tooltip" title="Voir la commande"><i class="la la-eye"></i> </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section("script")
    <script src="/assets/back/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        function table() {
            let table = $("#orders")

            table.DataTable({
                responsive: true,
            })
        }

        (function ($) {
            table();
        })(jQuery)
    </script>
@endsection