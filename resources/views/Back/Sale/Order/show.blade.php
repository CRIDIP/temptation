@extends("Back.Layout.app")

@section("style")

@endsection

@section("subheader")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">Commande N° {{ $order->id }} de {{ $order->user->name }}</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Ventes </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Commandes </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        Commande N° {{ $order->id }} </a>

                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
            <div class="kt-subheader__toolbar">
                <div class="kt-subheader__wrapper">
                    <a href="{{ route('Back.Order.index') }}" class="btn kt-subheader__btn-primary">
                        <i class="la la-angle-double-left"></i>
                        Retour &nbsp;
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">
                    <div class="kt-widget__media kt-hidden">
                        <img src="./assets/media/project-logos/3.png" alt="image">
                    </div>
                    <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden-">
                        {{ $order->id }}
                    </div>
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <a href="#" class="kt-widget__title">Commande N° {{ $order->id }}</a>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="flaticon-calendar"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Date</span>
                            <span class="kt-widget__value">{{ $order->dateOrder->format("d/m/Y") }}</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="flaticon-coins"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Total</span>
                            <span class="kt-widget__value">{{ formatCurrency($order->totalOrder) }}</span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="flaticon-open-box"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Produits</span>
                            <span class="kt-widget__value">{{ \App\Repository\Order\OrderProductRepository::staticCountProductForOrder($order->id) }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="la la-credit-card"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Commande <span
                                    class="kt-badge kt-badge--dark kt-badge--inline">{{ $order->numOrder }}</span> <span
                                    class="kt-badge kt-badge--dark kt-badge--inline">{{ $order->id }}</span>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#etat">
                                <i class="la la-clock-o"></i> Etat
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#documents">
                                <i class="la la-file"></i> Documents
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="etat" role="tabpanel">
                            <table class="table">
                                <tbody>
                                @foreach($order->statuses as $status)
                                    <tr class="{{ \App\Repository\Order\OrderRepository::stateOrderTableau($status->status, 'tableau') }}">
                                        <td><i class="{{ \App\Repository\Order\OrderRepository::stateOrderTableau($status->status, 'icon') }}"></i></td>
                                        <td>{{ \App\Repository\Order\OrderRepository::staticStateOrder($status->status, false, true) }}</td>
                                        <td>{{ $status->created_at->format("d/m/Y à H:i") }}</td>
                                        <td>
                                            <a href="" class="btn btn-secondary"><i class="la la-envelope"></i> Renvoyer l'email</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <form action="{{ route('Back.Order.upState', $order->id) }}" class="kt-form" method="post">
                                @csrf
                                @method("PUT")
                                <div class="form-group row">
                                    <select class="form-control col-md-8" name="upState">
                                        <option value=""></option>
                                        @foreach($statuses as $status)
                                            <option value="{{ $status["id"] }}">{{ $status["text"] }}</option>
                                        @endforeach
                                    </select>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-primary btn-block">Mettre à jour l'état</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="documents" role="tabpanel">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Document</th>
                                        <th>Numéro</th>
                                        <th>Montant</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $order->invoice->dateInvoice->format("d/m/Y à H:i") }}</td>
                                        <td>Facture</td>
                                        <td>{{ $order->invoice->numInvoice }}</td>
                                        <td>{{ formatCurrency($order->invoice->totalInvoice) }}</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <form action="{{ route('Back.Order.generateInvoice', $order->id) }}" class="kt-form" method="post">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-brand"><i class="la la-refresh"></i> Générer la facture</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a href="#livraison" class="nav-link active" data-toggle="tab">
                                <i class="la la-truck"></i> Livraison
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#return" class="nav-link active" data-toggle="tab">
                                <i class="la la-refresh"></i> Retour Produit
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="livraison" role="tabpanel">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Transporteur</th>
                                        <th>Poids</th>
                                        <th>Frais d'expédition</th>
                                        <th>Numéro de suivi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order->shippings as $shipping)
                                        <tr>
                                            <td>{{ $shipping->created_at->format("d/m/Y à H:i") }}</td>
                                            <td>{{ $shipping->shippement->name }}</td>
                                            <td>{{ $shipping->poids }} kg</td>
                                            <td>{{ formatCurrency($shipping->ship_amount) }}</td>
                                            <td>{{ $shipping->numSuivie }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-user"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Client <span
                                    class="kt-badge kt-badge--dark kt-badge--inline">{{ $order->user->name }}</span> <span
                                    class="kt-badge kt-badge--dark kt-badge--inline">{{ $order->user->id }}</span>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="well">
                                <strong>Email</strong><br>
                                <i class="la la-envelope"></i> <a href="mailto:{{ $order->user->email }}">{{ $order->user->email }}</a>
                            </div>
                            <br>
                            <div class="well">
                                <strong>Compte créer le</strong><br>
                                <i class="la la-calendar"></i> {{ $order->user->created_at->format('d/m/Y à H:i') }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="well">
                                <strong>Commande Valider</strong><br>
                                <span class="kt-badge kt-badge--brand">0</span>
                            </div>
                            <br>
                            <div class="well">
                                <strong>Total payé depuis la création du compte</strong><br>
                                <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded">0,00 €</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="la la-shopping-cart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Produits
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>Produit</th>
                    <th>Prix Unitaire</th>
                    <th>Quantité</th>
                    <th>Quantité Disponible</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->products as $product)
                    <tr>
                        <td>
                                <img src="/assets/images/product/{{ $product->product_id }}.jpg" width="150" />
                        </td>
                        <td>{{ $product->product->name }}</td>
                        <td>{{ formatCurrency($product->product->price) }}</td>
                        <td>{{ $product->qte }}</td>
                        <td>{{ $product->product->quantity }}</td>
                        <td>{{ formatCurrency($product->total) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <br>
            <hr>
            <br>
            <div class="row">
                <div class="col-md-7">
                    <div class="alert alert-warning" role="alert">
                        <div class="alert-icon"><i class="flaticon-exclamation"></i></div>
                        <div class="alert-text">Tout les produits afficher sont en TTC</div>
                    </div>
                </div>
                <div class="col-md-5">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td align="right">Produits:</td>
                                <td align="right">{{ formatCurrency($order->totalOrder-9.90) }}</td>
                            </tr>
                            <tr>
                                <td align="right">Transport:</td>
                                <td align="right">{{ formatCurrency(9.90) }}</td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight: bold;">Total:</td>
                                <td align="right" style="font-weight: bold;">{{ formatCurrency($order->totalOrder) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--<div class="kt-portlet__foot">
            <div class="row align-items-center">
                <div class="col-lg-6 m--valign-middle">
                    Portlet footer:
                </div>
                <div class="col-lg-6 kt-align-right">
                    <button type="submit" class="btn btn-brand">Submit</button>
                    <span class="kt-margin-left-10">or <a href="#" class="kt-link kt-font-bold">Cancel</a></span>
                </div>
            </div>
        </div>-->
    </div>
@endsection

@section("script")

@endsection