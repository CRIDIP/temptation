@extends("layouts.app")

@section("css")

@endsection

@section("content")
    <section class="section-two-columns">
        <div class="container">
            <div class="row-fluid">
                <div class="span3">
                    <div class="sidebar">
                        <div class="accordion-widget category-accordions">

                            <div class="accordion" >
                                @foreach(\App\Repository\Product\ProductSubCategoryRepository::getStaticSubcategoryByCategory($category->id) as $categorie)
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" href="{{ route('subcategorie', [$category->id, $categorie->id]) }}">
                                                {{ $categorie->name }}
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                        </div>

                        <hr>

                        <div class="accordion-widget filter-accordions">

                            <!--<div class="accordion" >
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse"  href="#collapse11">
                                            sort by
                                        </a>
                                    </div>
                                    <div id="collapse11" class="accordion-body collapse in">
                                        <div class="accordion-inner">

                                            <ul>
                                                <li><a href="#">popoular</a></li>
                                                <li><a href="#">lowest price</a></li>
                                                <li><a href="#">largest price</a></li>
                                                <li><a href="#">A-Z</a></li>
                                                <li><a href="#">Z-A</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse"  href="#collapse12">
                                            capacity
                                        </a>
                                    </div>
                                    <div id="collapse12" class="accordion-body collapse in">
                                        <div class="accordion-inner">

                                            <ul>
                                                <li><a href="#">30 ml</a></li>
                                                <li><a href="#">40 ml</a></li>
                                                <li><a href="#">60 ml</a></li>
                                                <li><a href="#">150 ml</a></li>
                                                <li><a href="#">200 ml</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>-->


                        </div>
                    </div>
                </div>
                <div class="span9">
                    <div class="products-list-head">
                        <h1>{{ $sub->name }}</h1>
                        <!--<div class="tag-line">
                            Nulla tellus arcu,<br>
                            fermentum et interdum eu...
                        </div>
                        <div class="image-holder">
                            <img alt="" src="images/woman.JPG" />
                        </div>-->
                    </div>

                    <div id="list-view" class="products-list products-holder  tab-pane">
                        @if(\App\Repository\Product\ProductRepository::countForSubcategory($sub->id) == 0)
                            <div class="alert alert-info" role="alert">
                                <h4 class="alert-heading">Information!</h4>
                                <p>Les produits de cette sous catégorie seront bientôt disponible.</p>
                            </div>
                        @endif
                        @foreach($products as $product)
                        <div class="list-item">
                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="thumb">
                                        <img alt="" src="/assets/images/product/{{ $product->id }}.jpg" />
                                    </div>
                                </div>
                                <div class="span8">
                                    <h1>{{ $product->name }}</h1>
                                    <div class="tag-line">
                                        {{ $product->recapitulatif }}
                                    </div>
                                    <div class="desc">
                                        {!! $product->description !!}
                                    </div>
                                    <div class="price">
                                        <span>{{ formatCurrency($product->price) }}</span>
                                        <!--<span class="old">$220.00</span>-->
                                    </div>
                                    <div class="buttons-holder">
                                        <a class="cusmo-btn add-button" href="#">Ajouter au panier</a>
                                        <a class="cusmo-btn gray add-button" href="{{ route('product', [$category->id, $product->id]) }}">Voir le produit</a>
                                        <a class="add-to-wishlist-btn" href="#"><i class="icon-plus"></i> Ajouter à ma liste</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @endforeach
                    </div>
                    <div id="grid-view" class="products-grid products-holder active tab-pane">
                        @if(\App\Repository\Product\ProductRepository::countForSubcategory($sub->id) == 0)
                            <div class="alert alert-info" role="alert">
                                <h4 class="alert-heading">Information!</h4>
                                <p>Les produits de cette sous catégorie seront bientôt disponible.</p>
                            </div>
                        @endif
                        <div class="row-fluid">
                            @foreach($products as $product)
                            <div class="span4" id="produit">
                                <div class="product-item">
                                    <a href="{{ route('product', [$category->id, $product->id]) }}">
                                        <img alt="" src="/assets/images/product/{{ $product->id }}.jpg" />
                                        <h1>{{ $product->name }}</h1>
                                    </a>
                                    <div class="tag-line">
                                        <span>{{ $product->recapitulatif }}</span>
                                    </div>
                                    <div class="price">
                                        {{ formatCurrency($product->price) }}
                                    </div>
                                    <form id="addProduct" action="{{ route('Cart.addProduct') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <button type="submit" id="btnAddProduct" class="cusmo-btn add-button" >Ajouter au panier</button>
                                    </form>

                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section("scripts")

@endsection()