@extends("layouts.app")

@section("css")

@endsection

@section("content")
    <section id="home" class="section-contact">
        <div class="container">
            <div class="row-fluid">
                <div class="span6">
                    <div class="form-holder right-border">
                        <h4>Vous êtes Client ?</h4>
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="control-group">

                                <div class="controls">
                                    <div class="form-label ">Email</div>

                                    <input type="text" id="email"   name="email"  class="required span12 cusmo-input"  />

                                </div>
                            </div>

                            <div class="control-group">

                                <div class="controls">
                                    <div class="form-label ">Mot de Passe</div>

                                    <input id="password" type="password"  name="password" class="required span12 cusmo-input"  />

                                </div>
                            </div>
                            <div class="rememberme">
                                <input type="checkbox" class="iCheck" > Remember me
                            </div>
                            <div class="forget-password">
                                <a href="#">Mot de Passe perdu ?</a>
                            </div>
                            <div class="button-holder">
                                <button class="cusmo-btn narrow " type="submit" >Me connecter</button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="span6">
                    <div class="form-holder">
                        <h4>Nouveau client</h4>
                        <form action="{{ route('register') }}" method="post">
                            @csrf
                            <div class="control-group">

                                <div class="controls">
                                    <div class="form-label ">Nom & Prénom</div>

                                    <input type="text"  name="name"  class="required span12 cusmo-input"  />

                                </div>
                            </div>
                            <div class="control-group">

                                <div class="controls">
                                    <div class="form-label ">Email</div>

                                    <input type="text"  name="email"  class="required span12 cusmo-input"  />

                                </div>
                            </div>

                            <div class="control-group">

                                <div class="controls">
                                    <div class="form-label ">Mot de passe</div>

                                    <input type="password"  name="password" class="required span12 cusmo-input"  />

                                </div>
                            </div>
                            <div class="control-group">

                                <div class="controls">
                                    <div class="form-label ">Confirmation du mot de passe</div>

                                    <input type="password"  name="confirm-password" class="required span12 cusmo-input"  />

                                </div>
                            </div>
                            <button class="cusmo-btn narrow pull-right" type="submit" >Créez mon compte</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")

@endsection()