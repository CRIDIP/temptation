<?php

namespace App\Model\Sale\Invoice;

use Illuminate\Database\Eloquent\Model;

class InvoicePaiement extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }

    public function getDates()
    {
        return ["datePaiement"];
    }
}
