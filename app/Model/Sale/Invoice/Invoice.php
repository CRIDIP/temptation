<?php

namespace App\Model\Sale\Invoice;

use App\Model\Sale\Order;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function products()
    {
        return $this->hasMany(InvoiceProduct::class);
    }

    public function paiements()
    {
        return $this->hasMany(InvoicePaiement::class);
    }

    public function getDates()
    {
        return ["dateInvoice"];
    }
}
