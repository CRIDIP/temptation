<?php

namespace App\Model\Sale\Order;

use App\Model\Sale\Order;
use Illuminate\Database\Eloquent\Model;

class OrderPaiement extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function getDates()
    {
        return ["datePaiement"];
    }
}
