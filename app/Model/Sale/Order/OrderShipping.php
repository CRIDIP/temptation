<?php

namespace App\Model\Sale\Order;

use App\Model\Checkout\Shipping;
use App\Model\Sale\Order;
use Illuminate\Database\Eloquent\Model;

class OrderShipping extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function shippement()
    {
        return $this->belongsTo(Shipping::class, 'shipping_id');
    }
}
