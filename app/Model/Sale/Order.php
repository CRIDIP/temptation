<?php

namespace App\Model\Sale;

use App\Model\Cart\Cart;
use App\Model\Sale\Invoice\Invoice;
use App\Model\Sale\Order\OrderPaiement;
use App\Model\Sale\Order\OrderProduct;
use App\Model\Sale\Order\OrderShipping;
use App\Model\Sale\Order\OrderStatus;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function paiements()
    {
        return $this->hasMany(OrderPaiement::class);
    }

    public function statuses()
    {
        return $this->hasMany(OrderStatus::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }

    public function shippings()
    {
        return $this->hasMany(OrderShipping::class);
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }

    public function getDates()
    {
        return ["dateOrder"];
    }
}
