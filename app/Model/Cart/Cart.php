<?php

namespace App\Model\Cart;

use App\Model\Sale\Order;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(CartProduct::class);
    }

    public function order()
    {
        return $this->hasOne(Order::class);
    }
}
