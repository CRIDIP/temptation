<?php

namespace App\Model\Checkout;

use Illuminate\Database\Eloquent\Model;

class CheckoutStatus extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
