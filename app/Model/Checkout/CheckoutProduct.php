<?php

namespace App\Model\Checkout;

use Illuminate\Database\Eloquent\Model;

class CheckoutProduct extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
