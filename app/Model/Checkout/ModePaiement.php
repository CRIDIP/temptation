<?php

namespace App\Model\Checkout;

use Illuminate\Database\Eloquent\Model;

class ModePaiement extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
