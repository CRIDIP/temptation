<?php

namespace App\Model\Checkout;

use App\Model\Sale\Order;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
