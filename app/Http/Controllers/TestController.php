<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Cart\CartController;
use App\Repository\Cart\CartRepository;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function code(CartController $cartController)
    {
        dd(CartRepository::getStaticCart());
    }
}
