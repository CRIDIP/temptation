<?php

namespace App\Http\Controllers\Checkout;

use App\HelperClass\Checkout;
use App\HelperClass\Order;
use App\Mail\NewCommandForUser;
use App\Notifications\NewCommand;
use App\Repository\Cart\CartProductRepository;
use App\Repository\Cart\CartRepository;
use App\Repository\Checkout\CheckoutPaiementRepository;
use App\Repository\Checkout\CheckoutProductRepository;
use App\Repository\Checkout\CheckoutRepository;
use App\Repository\Checkout\CheckoutShippingRepository;
use App\Repository\Product\ProductRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;

class CheckoutController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var CartProductRepository
     */
    private $cartProductRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var Checkout
     */
    private $checkout;
    /**
     * @var Order
     */
    private $order;


    /**
     * CheckoutController constructor.
     * @param CartRepository $cartRepository
     * @param CartProductRepository $cartProductRepository
     * @param ProductRepository $productRepository
     * @param Checkout $checkout
     * @param Order $order
     */
    public function __construct(
        CartRepository $cartRepository,
        CartProductRepository $cartProductRepository,
        ProductRepository $productRepository,
        Checkout $checkout,
        Order $order
    )
    {
        $this->middleware(["auth"]);
        $this->cartRepository = $cartRepository;
        $this->cartProductRepository = $cartProductRepository;
        $this->productRepository = $productRepository;
        $this->checkout = $checkout;
        $this->order = $order;
    }

    public function index()
    {
        //dd(auth()->user()->adresse);
        return view("Checkout.index", [
            "cart"  => $this->cartRepository->get(session()->get('_token'))
        ]);
    }

    public function checkout(Request $request)
    {
        //dd(json_decode($request->cart));

        $cart = $request->cart;

        $cart = $this->cartRepository->get(\session()->get('_token'));
        $user = User::find('1');

        $stripe = $this->checkout->StripeCheckout(
            $request->total,
            $request->num_panier,
            $request->stripeToken,
            $request->email,
            $request->name,
            $request->adresse,
            $request->code_postal,
            $request->ville,
            $request->telephone
        );

        if($stripe[0]['state'] == 'OK'){
            $status = 10;
        }else{
            $status = 7;
        }

        $checkout = $this->checkout->addCheckout($cart->id,auth()->user()->id,$status);
        //dd($checkout);
        $order = $this->order->addOrder(auth()->user()->id,$request->total, $status, $cart->id);
        $status = $this->order->addOrderStatus($order->id, $status);
        //dd($order);


        $customer = [
            "name"  => $request->name,
            "email" => $request->email,
            "address" => [
                "line"  => $request->adresse,
                "codePostal"    => $request->code_postal,
                "ville" => $request->ville
            ],
            "phone" => $request->phone
        ];

        $invoice = [
            "num"   => $cart->id,
            "transport" => 9.90,
            "subtotal"  => $request->total-9.90,
            "total" => $request->total,
        ];

        $items = [];

        foreach ($cart->products as $product)
        {
            $items[] = [
                "id"            => $product->product_id,
                "designation"   => $product->product->name,
                "reference"     => $product->product->reference,
                "qte"           => $product->qte,
                "price"         => $product->product->price,
                "amount"        => $product->total_price
            ];
        }

        $products = $this->checkout->addCheckoutProducts($items, $checkout->id);
        $paiement = $this->checkout->addCheckoutPaiement($checkout->id,2,$request->total);
        $shipping = $this->checkout->addCheckoutShippement($checkout->id, 1, 0, 9.90);

        $orderPoduct = $this->order->addOrderProduct($order->id,$items);
        $orderPaiement = $this->order->addOrderPaiement($order->id,2, "PM".generateNum(9), $request->total);
        $orderShipping = $this->order->addOrderShippement($order->id, 0, 9.90);


        Mail::to($request->email)->send(new NewCommandForUser($customer, $invoice, $items));
        Notification::send($user, new NewCommand($customer, $invoice, $items));

        Session::flash('success', "Le Paiement à été exécuter et votre commande est en attente de validation.<br>Vous allez recevoir un mail de confirmation");

        return back();
    }

}
