<?php

namespace App\Http\Controllers\Back\Sale\Order;

use App\HelperClass\Invoice;
use App\Repository\Order\OrderRepository;
use App\Repository\Order\OrderStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Kamaln7\Toastr\Facades\Toastr;

class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var OrderStatusRepository
     */
    private $orderStatusRepository;
    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * OrderController constructor.
     * @param OrderRepository $orderRepository
     * @param OrderStatusRepository $orderStatusRepository
     * @param Invoice $invoice
     */
    public function __construct(OrderRepository $orderRepository, OrderStatusRepository $orderStatusRepository, Invoice $invoice)
    {
        $this->orderRepository = $orderRepository;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->invoice = $invoice;
    }

    public function orderMonth()
    {
        $sumJan = $this->orderRepository->sumBeetweenDate('2019-01-01', '2019-01-31');
        $sumFev = $this->orderRepository->sumBeetweenDate('2019-02-01', '2019-02-28');
        $sumMars = $this->orderRepository->sumBeetweenDate('2019-03-01', '2019-03-31');
        $sumAvril = $this->orderRepository->sumBeetweenDate('2019-04-01', '2019-04-30');
        $sumMai = $this->orderRepository->sumBeetweenDate('2019-05-01', '2019-05-31');
        $sumJuin = $this->orderRepository->sumBeetweenDate('2019-06-01', '2019-06-30');
        $sumJuillet = $this->orderRepository->sumBeetweenDate('2019-07-01', '2019-07-31');
        $sumAout = $this->orderRepository->sumBeetweenDate('2019-08-01', '2019-08-31');
        $sumSeptembre = $this->orderRepository->sumBeetweenDate('2019-09-01', '2019-09-30');
        $sumOctobre = $this->orderRepository->sumBeetweenDate('2019-10-01', '2019-10-31');
        $sumNovembre = $this->orderRepository->sumBeetweenDate('2019-11-01', '2019-11-30');
        $sumDecembre = $this->orderRepository->sumBeetweenDate('2019-12-01', '2019-12-31');

        $tab = [$sumJan, $sumFev, $sumMars, $sumAvril, $sumMai, $sumJuin, $sumJuillet, $sumAout, $sumSeptembre, $sumOctobre, $sumNovembre, $sumDecembre];
        $labels = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];

        return response()->json(["table" => $tab, "labels" => $labels]);
    }

    public function listOrders()
    {
        $datas = $this->orderRepository->getAll();
        $data = [];

        foreach ($datas as $item) {
            $data[] = [
                "orderId" => $item->id,
                "numOrder" => $item->id,
                "customer" => $item->user->name,
                "dateOrder" => $item->dateOrder->format("d/m/Y"),
                "montantOrder" => formatCurrency($item->totalOrder),
                "stateOrder" => $item->stateOrder,
                ""
            ];
        }

        return response()->json(["data" => $data]);
    }

    public function index()
    {
        return view("Back.Sale.Order.index", [
            "orders" => $this->orderRepository->getAll()
        ]);
    }

    public function show($orderId)
    {
        return view("Back.Sale.Order.show", [
            "order" => $this->orderRepository->get($orderId),
            "statuses" => $this->orderRepository->getAllStatusesPluck()
        ]);
    }

    public function upState($orderId, Request $request)
    {
        try {
            $order = $this->orderRepository->upState($orderId, $request->upState);
            $status = $this->orderStatusRepository->create($orderId, $request->upState);


            Toastr::success("Le Status de la commande " . $order->numOrder . " à été mis à jour", "Mise à jour de l'état de la commande");
            return back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }

    public function generateInvoice($orderId, Request $request)
    {
        $order = $this->orderRepository->get($orderId);

        try {
            $invoice = $this->invoice->addInvoice(
                $order->id,
                $order->customer_id,
                $order->totalOrder,
                $order->stateOrder
            );

            foreach ($order->products as $product) {
                $this->invoice->addInvoiceProduct($invoice->id, $product->product_id, $product->qte, $product->total);
            }

            foreach ($order->paiements as $paiement) {
                $this->invoice->addInvoicePaiement($invoice->id, 2, $order->totalOrder);
            }

            Toastr::success("La Facture N°".$invoice->numInvoice." à été générer", "Génération d'une facture");
            return back();
        } catch (\Exception $exception) {
            Log::error($exception->getMessage(), ["code" => $exception->getCode()]);
            Toastr::error("Erreur de traitement, consulter les logs", "Erreur 500");
            return redirect()->back();
        }
    }
    
}
