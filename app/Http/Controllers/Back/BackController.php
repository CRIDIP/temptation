<?php

namespace App\Http\Controllers\Back;

use App\Repository\Order\OrderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * BackController constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function dashboard()
    {
        //dd($this->orderRepository->getLastOrder());
        return view("Back.dashboard", [
            "orders"    => $this->orderRepository->getLastOrder()
        ]);
    }
}
