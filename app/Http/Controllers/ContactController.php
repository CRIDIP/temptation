<?php

namespace App\Http\Controllers;

use App\Mail\NewContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function __construct()
    {
    }

    public function contact()
    {
        return view('contact');
    }

    public function contactPost(Request $request){
        $data = $request->validate([
            "name"  => "required|min:2",
            "email" => "required|email",
            "type"  => "required",
            "fct_number" => "",
            "commentaire" => "required|min:25"
        ]);

        Mail::to('contact@ouibienetre.com')->send(new NewContact($data));

        return back()->with('success', "Votre demande de contact à été envoyer, nous y répondrons le plus rapidement possible !");
    }
}
