<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCommandForUser extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    public $customer;
    /**
     * @var array
     */
    public $invoice;
    /**
     * @var array
     */
    public $items;

    /**
     * Create a new message instance.
     *
     * @param array $customer
     * @param array $invoice
     * @param array $items
     */
    public function __construct(array $customer, array $invoice, array $items)
    {
        //
        $this->customer = (object)$customer;
        $this->invoice = (object)$invoice;
        $this->items = (object)$items;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("commande@ouibientre.com")
            ->view('Email.Checkout.UserCheckout')->subject("Votre commande N°".$this->invoice->num);
    }
}
