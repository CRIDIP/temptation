<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewCommand extends Notification
{
    use Queueable;
    private $customer;
    private $invoice;
    private $items;

    /**
     * Create a new notification instance.
     *
     * @param $customer
     * @param $invoice
     * @param $items
     */
    public function __construct($customer, $invoice, $items)
    {
        //
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->items = $items;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->success()
                    ->subject("Nouvelle commande")
            ->line("Une commande à été passer sur le site")
            ->line('Numero de la commande: '.$this->invoice['num'])
            ->line('Client: '.$this->customer['name'])
            ->line('Total de la commande:'.formatCurrency($this->invoice['total']))
            ->line('Date:'.now());
    }

    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->success()
            ->from('Commande')
            ->image(env("APP_URL").'/assets/images/logo.png')
            ->content("Une commande à été passer sur le site")
            ->attachment(function ($attachment) {
                $attachment->title("Commande N°".$this->invoice['num'])
                    ->fields([
                        "Client" => $this->customer['name'],
                        "Total de la commande" => formatCurrency($this->invoice['total']),
                        "Paiement" => ":+1",
                        "Date"  => now()
                    ]);
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
