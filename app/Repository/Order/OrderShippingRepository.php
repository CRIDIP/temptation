<?php
namespace App\Repository\Order;

use App\Model\Sale\Order\OrderShipping;

class OrderShippingRepository
{
    /**
     * @var OrderShipping
     */
    private $orderShipping;

    /**
     * OrderShippingRepository constructor.
     * @param OrderShipping $orderShipping
     */

    public function __construct(OrderShipping $orderShipping)
    {
        $this->orderShipping = $orderShipping;
    }

    public function create($order_id, $pds, $amount)
    {
        return $this->orderShipping->newQuery()
            ->create([
                "order_id"  => $order_id,
                "shipping_id" => 1,
                "poids"     => $pds,
                "ship_amount"=> $amount
            ]);
    }

}

        