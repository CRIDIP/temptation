<?php
namespace App\Repository\Order;

use App\Model\Sale\Order\OrderProduct;

class OrderProductRepository
{
    /**
     * @var OrderProduct
     */
    private $orderProduct;

    /**
     * OrderProductRepository constructor.
     * @param OrderProduct $orderProduct
     */

    public function __construct(OrderProduct $orderProduct)
    {
        $this->orderProduct = $orderProduct;
    }

    public static function staticCountProductForOrder($order_id){
        $orderProduct = new OrderProduct();
        return $orderProduct->newQuery()->where('order_id', $order_id)->get()->count();
    }

    public function insert($order_id, $id, $qte, $amount)
    {
        return $this->orderProduct->newQuery()
            ->create([
                "order_id"  => $order_id,
                "product_id"=> $id,
                "qte"       => $qte,
                "total"     => $amount
            ]);
    }

}

        