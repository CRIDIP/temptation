<?php
namespace App\Repository\Order;

use App\Model\Sale\Order\OrderStatus;

class OrderStatusRepository
{
    /**
     * @var OrderStatus
     */
    private $orderStatus;

    /**
     * OrderStatusRepository constructor.
     * @param OrderStatus $orderStatus
     */

    public function __construct(OrderStatus $orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    public function create($orderId, $upState)
    {
        return $this->orderStatus->newQuery()
            ->create([
                "order_id"  => $orderId,
                "status"    => $upState
            ]);
    }

}

        