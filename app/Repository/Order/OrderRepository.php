<?php

namespace App\Repository\Order;

use App\Model\Sale\Order;

class OrderRepository
{
    /**
     * @var Order
     */
    private $order;

    /**
     * OrderRepository constructor.
     * @param Order $order
     */

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public static function staticCountOrder()
    {
        $order = new Order();
        return $order->newQuery()->get()->count();
    }

    public static function getLastOrderDate()
    {
        $order = new Order();
        $data = $order->newQuery()->get()->last();
        return $data->dateOrder->format('d/m/Y');
    }

    public static function staticSumOrder()
    {
        $order = new Order();
        $data = $order->newQuery()->sum('totalOrder');
        return $data;
    }

    public static function staticMoyOrder()
    {
        $order = new Order();
        $total = $order->newQuery()->sum('totalOrder');
        $count = $order->newQuery()->get()->count();

        return $total / $count;
    }


    public function sumBeetweenDate(string $string, string $string1)
    {
        return $this->order->newQuery()->whereBetween('dateorder', [$string, $string1])->sum('totalOrder');
    }

    public function getLastOrder()
    {
        return $this->order->newQuery()->orderByDesc('dateOrder')->limit(10)->get()->load('user', 'products', 'paiements');
    }

    public static function stateOrderTableau($state, $env)
    {
        if ($env == 'tableau') {
            switch ($state) {
                case 0:
                    return 'table-active';
                    break;
                case 1:
                    return 'table-danger';
                    break;
                case 2:
                    return 'table-warning';
                    break;
                case 3:
                    return 'table-warning';
                    break;
                case 4:
                    return 'table-warning';
                    break;
                case 5:
                    return 'table-warning';
                    break;
                case 6:
                    return 'table-info';
                    break;
                case 7:
                    return 'table-danger';
                    break;
                case 8:
                    return 'table-info';
                    break;
                case 9:
                    return 'table-success';
                    break;
                case 10:
                    return 'table-success';
                    break;
                case 11:
                    return 'table-success';
                    break;
                default:
                    return 'table-brand';
                    break;
            }
        } elseif ($env == 'icon') {
            switch ($state) {
                case 0:
                    return 'la la-refresh';
                    break;
                case 1:
                    return 'la la-times-circle';
                    break;
                case 2:
                    return 'la la-clock-o';
                    break;
                case 3:
                    return 'la la-clock-o';
                    break;
                case 4:
                    return 'la la-clock-o';
                    break;
                case 5:
                    return 'la la-clock-o';
                    break;
                case 6:
                    return 'la la-dropbox';
                    break;
                case 7:
                    return 'la la-warning';
                    break;
                case 8:
                    return 'la la-truck';
                    break;
                case 9:
                    return 'la la-check-circle';
                    break;
                case 10:
                    return 'la la-credit-card';
                    break;
                case 11:
                    return 'la la-exchange';
                    break;
                default:
                    return 'la la-exclamation-circle';
                    break;
            }
        } else {
            return null;
        }
    }

    public static function staticStateOrder(int $state, $labeled = false, $texted = false)
    {
        if ($labeled == true && $texted == false) {
            switch ($state) {
                case 0:
                    return "<span class='kt-badge kt-badge--inline kt-badge--brand kt-badge--lg'><i class='la la-refresh'></i> Commande en cours</span>";
                    break;
                case 1:
                    return "<span class='kt-badge kt-badge--inline kt-badge--danger kt-badge--lg'><i class='la la-times-circle'></i> Annulé</span>";
                    break;
                case 2:
                    return "<span class='kt-badge kt-badge--inline kt-badge--warning kt-badge--lg'><i class='la la-clock-o'></i> En attente de paiement à la livraison</span>";
                    break;
                case 3:
                    return "<span class='kt-badge kt-badge--inline kt-badge--warning kt-badge--lg'><i class='la la-clock-o'></i> En attente de réapprovisionnement (Non Payé)</span>";
                    break;
                case 4:
                    return "<span class='kt-badge kt-badge--inline kt-badge--warning kt-badge--lg'><i class='la la-clock-o'></i> En attente de réapprovisionnement (Payé)</span>";
                    break;
                case 5:
                    return "<span class='kt-badge kt-badge--inline kt-badge--warning kt-badge--lg'><i class='la la-bank'></i> En attente de virement bancaire</span>";
                    break;
                case 6:
                    return "<span class='kt-badge kt-badge--inline kt-badge--info kt-badge--lg'><i class='la la-dropbox'></i> En cours de préparation</span>";
                    break;
                case 7:
                    return "<span class='kt-badge kt-badge--inline kt-badge--danger kt-badge--lg'><i class='la la-warning'></i> Erreur de paiement</span>";
                    break;
                case 8:
                    return "<span class='kt-badge kt-badge--inline kt-badge--info kt-badge--lg'><i class='la la-truck'></i> Expedié</span>";
                    break;
                case 9:
                    return "<span class='kt-badge kt-badge--inline kt-badge--success kt-badge--lg'><i class='la la-check-circle'></i> Livré</span>";
                    break;
                case 10:
                    return "<span class='kt-badge kt-badge--inline kt-badge--success kt-badge--lg'><i class='la la-credit-card'></i> Paiement Accepté</span>";
                    break;
                case 11:
                    return "<span class='kt-badge kt-badge--inline kt-badge--success kt-badge--lg'><i class='la la-exchange'></i> Remboursé</span>";
                    break;
                default:
                    return "<span class='kt-badge kt-badge--inline kt-badge--dark kt-badge--lg'><i class='la la-exclamation-circle'></i> Inconnue</span>";
                    break;
            }
        } elseif ($labeled == false && $texted == true) {
            switch ($state) {
                case 0:
                    return 'Commande en cours';
                case 1:
                    return 'Annulé';
                case 2:
                    return 'En attente de paiement à la livraison';
                case 3:
                    return 'En attente de réapprovisionnement (Non Payé)';
                case 4:
                    return 'En attente de réapprovisionnement (Payé)';
                case 5:
                    return 'En attente de virement bancaire';
                case 6:
                    return 'En cours de préparation';
                case 7:
                    return 'Erreur de Paiement';
                case 8:
                    return 'Expédié';
                case 9:
                    return 'Livré';
                case 10:
                    return 'Paiement Accepté';
                case 11:
                    return 'Remboursé';
                default:
                    return 'Inconnue';
            }
        } else {
            switch ($state) {
                case 0:
                    return 'kt-badge--brand';
                case 1:
                    return 'kt-badge--danger';
                case 2:
                    return 'kt-badge--warning';
                case 3:
                    return 'kt-badge--warning';
                case 4:
                    return 'kt-badge--warning';
                case 5:
                    return 'kt-badge--warning';
                case 6:
                    return 'kt-badge--info';
                case 7:
                    return 'kt-badge--danger';
                case 8:
                    return 'kt-badge--info';
                case 9:
                    return 'kt-badge--success';
                case 10:
                    return 'kt-badge--success';
                case 11:
                    return 'kt-badge--success';
                default:
                    return 'kt-badge--dark';
            }
        }
    }

    public function getAllStatusesPluck()
    {
        $tab = [
            ["id"    => 0, "text"  => "Commande en cours"],
            ["id"    => 1, "text"  => "Annulé"],
            ["id"    => 2, "text"  => "En attente de paiement à la livraison"],
            ["id"    => 3, "text"  => "En attente de réapprovisionnement (Non Payé)"],
            ["id"    => 4, "text"  => "En attente de réapprovisionnement (Payé)"],
            ["id"    => 5, "text"  => "En attente de virement bancaire"],
            ["id"    => 6, "text"  => "En cours de préparation"],
            ["id"    => 7, "text"  => "Erreur de Paiement"],
            ["id"    => 8, "text"  => "Expédié"],
            ["id"    => 9, "text"  => "Livré"],
            ["id"    => 10, "text"  => "Paiement Accepté"],
            ["id"    => 11, "text"  => "Remboursé"],
        ];

        return $tab;
    }

    public function getAll()
    {
        return $this->order->newQuery()->get()->load('user', 'products', 'paiements');
    }

    public function create($customer_id, $total, $status, $cart_id)
    {
        $order = $this->order->newQuery()->orderByDesc('id')->first();
        if ($order == null) {
            $numOrder = "CMD1";
        } else {
            $numOrder = "CMD" . $order->id++;
        }

        return $this->order->newQuery()
            ->create([
                "customer_id" => $customer_id,
                "numOrder" => $numOrder,
                "dateOrder" => now(),
                "totalOrder" => $total,
                "stateOrder" => $status,
                "cart_id"   => $cart_id
            ]);
    }

    public function get($orderId)
    {
        return $this->order->newQuery()
            ->find($orderId)->load('user', 'products', 'paiements', 'statuses');
    }

    public function upState($orderId, $upState)
    {
        $this->order->newQuery()->find($orderId)->update([
            "stateOrder"    => $upState
        ]);

        return $this->get($orderId);
    }

}

        