<?php
namespace App\Repository\Order;

use App\Model\Sale\Order\OrderPaiement;

class OrderPaiementRepository
{
    /**
     * @var OrderPaiement
     */
    private $orderPaiement;

    /**
     * OrderPaiementRepository constructor.
     * @param OrderPaiement $orderPaiement
     */

    public function __construct(OrderPaiement $orderPaiement)
    {
        $this->orderPaiement = $orderPaiement;
    }

    public function create($order_id, $modePaiement, $numPaiement, $montantPaiement)
    {
        return $this->orderPaiement->newQuery()
            ->create([
                "order_id"  => $order_id,
                "modePaiement" => $modePaiement,
                "numPaiement" => $numPaiement,
                "datePaiement"  => now(),
                "montantPaiement" => $montantPaiement
            ]);
    }

}

        