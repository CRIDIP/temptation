<?php
namespace App\Repository\Product;

use App\Model\Product\ProductSubCategory;

class ProductSubCategoryRepository
{
    /**
     * @var ProductSubCategory
     */
    private $productSubCategory;

    /**
     * ProductSubCategoryRepository constructor.
     * @param ProductSubCategory $productSubCategory
     */

    public function __construct(ProductSubCategory $productSubCategory)
    {
        $this->productSubCategory = $productSubCategory;
    }

    public static function getStaticSubcategoryByCategory($category_id)
    {
        $sub = new ProductSubCategory();
        return $sub->newQuery()->where('category_id', $category_id)->get();
    }

    public function get($sub_id)
    {
        return $this->productSubCategory->newQuery()
            ->find($sub_id);
    }

}

        