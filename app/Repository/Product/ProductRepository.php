<?php
namespace App\Repository\Product;

use App\Model\Product\Product;

class ProductRepository
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */

    public function __construct(Product $product)
    {
        $this->product = $product;
    }


    public static function staticAll()
    {
        $product = new Product();

        return $product->newQuery()->limit(4)->orderByDesc('created_at')->get()->load('category', 'subcategory');
    }

    public function getForCategory($category_id)
    {
        return $this->product->newQuery()->where('category_id', $category_id)->get();
    }

    public function get($product_id)
    {
        return $this->product->newQuery()->find($product_id)->load('category', 'subcategory');
    }

    public function getForSubcategorie($sub_id)
    {
        return $this->product->newQuery()->where('subcategory_id', $sub_id)->get();
    }

    public static function countForCategory($cat_id)
    {
        $product = new Product();

        return $product->newQuery()->where('category_id', $cat_id)->get()->count();
    }

    public static function countForSubcategory($sub_id)
    {
        $product = new Product();

        return $product->newQuery()->where('subcategory_id', $sub_id)->get()->count();
    }
}

        