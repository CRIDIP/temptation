<?php
namespace App\Repository\Account;

use App\Model\Account\UserNote;

class UserNoteRepository
{
    /**
     * @var UserNote
     */
    private $userNote;

    /**
     * UserNoteRepository constructor.
     * @param UserNote $userNote
     */

    public function __construct(UserNote $userNote)
    {
        $this->userNote = $userNote;
    }

}

