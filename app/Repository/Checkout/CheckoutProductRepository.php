<?php
namespace App\Repository\Checkout;

use App\Model\Checkout\CheckoutProduct;

class CheckoutProductRepository
{
    /**
     * @var CheckoutProduct
     */
    private $checkoutProduct;

    /**
     * CheckoutProductRepository constructor.
     * @param CheckoutProduct $checkoutProduct
     */

    public function __construct(CheckoutProduct $checkoutProduct)
    {
        $this->checkoutProduct = $checkoutProduct;
    }

    public function insert($checkout_id, $id, $qte, $amount)
    {
        return $this->checkoutProduct->newQuery()
            ->create([
                "checkout_id"   => $checkout_id,
                "product_id"    => $id,
                "qte"           => $qte,
                "total_price"   => $amount
            ]);
    }

}

        