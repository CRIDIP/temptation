<?php
namespace App\Repository\Checkout;

use App\Model\Checkout\CheckoutShipping;

class CheckoutShippingRepository
{
    /**
     * @var CheckoutShipping
     */
    private $checkoutShipping;

    /**
     * CheckoutShippingRepository constructor.
     * @param CheckoutShipping $checkoutShipping
     */

    public function __construct(CheckoutShipping $checkoutShipping)
    {
        $this->checkoutShipping = $checkoutShipping;
    }

    public function create($checkout_id, $shipping_id, $pds, $ship_amount, $suivi)
    {
        return $this->checkoutShipping->newQuery()
            ->create([
                "checkout_id"   => $checkout_id,
                "shipping_id"   => $shipping_id,
                "poids"         => $pds,
                "ship_amount"   => $ship_amount,
                "numSuivie"     => $suivi
            ]);
    }

}

        