<?php
namespace App\Repository\Checkout;

use App\Model\Checkout\CheckoutPaiement;

class CheckoutPaiementRepository
{
    /**
     * @var CheckoutPaiement
     */
    private $checkoutPaiement;

    /**
     * CheckoutPaiementRepository constructor.
     * @param CheckoutPaiement $checkoutPaiement
     */

    public function __construct(CheckoutPaiement $checkoutPaiement)
    {
        $this->checkoutPaiement = $checkoutPaiement;
    }

    public function create($checkout_id, $mode_id, $total_paiement)
    {
        return $this->checkoutPaiement->newQuery()
            ->create([
                "checkout_id"   => $checkout_id,
                "mode_id"       => $mode_id,
                "datePaiement"  => now(),
                "reference"     => "PM".$checkout_id,
                "total_paiement"=> $total_paiement
            ]);
    }

}

        