<?php
namespace App\Repository\Checkout;

use App\Model\Checkout\Checkout;

class CheckoutRepository
{
    /**
     * @var Checkout
     */
    private $checkout;

    /**
     * CheckoutRepository constructor.
     * @param Checkout $checkout
     */

    public function __construct(Checkout $checkout)
    {
        $this->checkout = $checkout;
    }

    public function create($cart_id, $user_id, $status)
    {
        return $this->checkout->newQuery()
            ->create([
                "reference" => "CHK".$cart_id,
                "cart_id"   => $cart_id,
                "user_id"   => $user_id,
                "dateCheckout" => now(),
                "status_id" => $status
            ]);
    }

}

        