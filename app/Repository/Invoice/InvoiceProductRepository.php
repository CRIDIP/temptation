<?php
namespace App\Repository\Invoice;

use App\Model\Sale\Invoice\InvoiceProduct;

class InvoiceProductRepository
{
    /**
     * @var InvoiceProduct
     */
    private $invoiceProduct;

    /**
     * InvoiceProductRepository constructor.
     * @param InvoiceProduct $invoiceProduct
     */

    public function __construct(InvoiceProduct $invoiceProduct)
    {
        $this->invoiceProduct = $invoiceProduct;
    }

    public function create($invoice_id, $product_id, $qte, $total)
    {
        return $this->invoiceProduct->newQuery()
            ->create([
                "invoice_id"    => $invoice_id,
                "product_id"    => $product_id,
                "qte"           => $qte,
                "total"         => $total
            ]);
    }

}

        