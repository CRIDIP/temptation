<?php
namespace App\Repository\Invoice;

use App\Model\Sale\Invoice\InvoicePaiement;

class InvoicePaiementRepository
{
    /**
     * @var InvoicePaiement
     */
    private $invoicePaiement;

    /**
     * InvoicePaiementRepository constructor.
     * @param InvoicePaiement $invoicePaiement
     */

    public function __construct(InvoicePaiement $invoicePaiement)
    {
        $this->invoicePaiement = $invoicePaiement;
    }

    public function create($invoice_id, $modePaiement, $montantPaiement)
    {
        return $this->invoicePaiement->newQuery()
            ->create([
                "invoice_id"    => $invoice_id,
                "modePaiement"  => $modePaiement,
                "numPaiement"   => "PM00000".$invoice_id,
                "datePaiement"  => now(),
                "montantPaiement"=> $montantPaiement
            ]);
    }

}

        