<?php
namespace App\Repository\Invoice;

use App\Model\Sale\Invoice\Invoice;

class InvoiceRepository
{
    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * InvoiceRepository constructor.
     * @param Invoice $invoice
     */

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function create($id, $user_id, $totalOrder, $stateOrder)
    {
        return $this->invoice->newQuery()
            ->create([
                "order_id"  => $id,
                "user_id"   => $user_id,
                "numInvoice" => "FCT0000".$id,
                "dateInvoice"=> now(),
                "totalInvoice"  => $totalOrder,
                "stateInvoice"  => $stateOrder
            ]);
    }

}

        