<?php
namespace App\Repository\Cart;

use App\Model\Cart\CartProduct;

class CartProductRepository
{
    /**
     * @var CartProduct
     */
    private $cartProduct;

    /**
     * CartProductRepository constructor.
     * @param CartProduct $cartProduct
     */

    public function __construct(CartProduct $cartProduct)
    {
        $this->cartProduct = $cartProduct;
    }

    public function product_exist($id, $product_id)
    {
        $cartProduct = $this->cartProduct->newQuery()->where('cart_id', $id)->where('product_id', $product_id)->get()->count();
        if($cartProduct == 0)
        {
            return false;
        }else{
            return true;
        }
    }

    public function get($id, $product_id)
    {
        return $this->cartProduct->newQuery()->where('cart_id', $id)->where('product_id', $product_id)->first();
    }

    public function updateProduct($id, $product_id, int $qte, float $total_price)
    {
        return $this->cartProduct->newQuery()->where('cart_id', $id)
            ->where('product_id', $product_id)
            ->first()
            ->update([
                "qte"   => $qte,
                "total_price" => $total_price
            ]);
    }

    public function createProduct($id, $product_id, int $qte, float $total_price)
    {
        return $this->cartProduct->newQuery()->create([
            "cart_id"       => $id,
            "product_id"    => $product_id,
            "qte"           => $qte,
            "total_price"   => $total_price
        ]);
    }

    public function getById($product)
    {
        return $this->cartProduct->newQuery()->find($product)->load('product');
    }

    public static function countStaticProduct($cart_id)
    {
        $product = new CartProduct();
        $count = $product->newQuery()->where('cart_id', $cart_id)->get()->count();
        return $count;
    }

    public function delete($product_id)
    {
        return $this->cartProduct->newQuery()->find($product_id)->delete();
    }

    public function destroyForCart($cart_id)
    {
        return $this->cartProduct->newQuery()
            ->where('cart_id', $cart_id)
            ->delete();
    }

}

        