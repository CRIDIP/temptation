<?php

namespace App;

use App\Model\Account\UserAdress;
use App\Model\Account\UserInfo;
use App\Model\Account\UserLogin;
use App\Model\Account\UserNote;
use App\Model\Sale\Invoice\Invoice;
use App\Model\Sale\Order;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'group'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function logins()
    {
        return $this->hasMany(UserLogin::class);
    }

    public function note()
    {
        return $this->hasOne(UserNote::class);
    }

    public function adresses()
    {
        return $this->hasMany(UserAdress::class);
    }

    public function adresse()
    {
        return $this->hasOne(UserAdress::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
