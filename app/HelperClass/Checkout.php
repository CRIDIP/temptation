<?php


namespace App\HelperClass;


use App\Repository\Checkout\CheckoutPaiementRepository;
use App\Repository\Checkout\CheckoutProductRepository;
use App\Repository\Checkout\CheckoutRepository;
use App\Repository\Checkout\CheckoutShippingRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;

class Checkout
{
    /**
     * @var CheckoutRepository
     */
    private $checkoutRepository;
    /**
     * @var CheckoutProductRepository
     */
    private $checkoutProductRepository;
    /**
     * @var CheckoutShippingRepository
     */
    private $checkoutShippingRepository;
    /**
     * @var CheckoutPaiementRepository
     */
    private $checkoutPaiementRepository;

    /**
     * Checkout constructor.
     * @param CheckoutRepository $checkoutRepository
     * @param CheckoutProductRepository $checkoutProductRepository
     * @param CheckoutShippingRepository $checkoutShippingRepository
     * @param CheckoutPaiementRepository $checkoutPaiementRepository
     */
    public function __construct(CheckoutRepository $checkoutRepository, CheckoutProductRepository $checkoutProductRepository, CheckoutShippingRepository $checkoutShippingRepository, CheckoutPaiementRepository $checkoutPaiementRepository)
    {

        $this->checkoutRepository = $checkoutRepository;
        $this->checkoutProductRepository = $checkoutProductRepository;
        $this->checkoutShippingRepository = $checkoutShippingRepository;
        $this->checkoutPaiementRepository = $checkoutPaiementRepository;
    }

    public function StripeCheckout($total, $num_panier, $stripeToken, $email, $name, $adresse, $code_postal, $ville, $telephone)
    {
        $status = [];
        try {
            Stripe::setApiKey(env("STRIPE_SECRET"));

            $charge = Charge::create([
                "amount" => $total * 100,
                "currency" => "eur",
                "description" => "Facture N°" . $num_panier,
                "source" => $stripeToken,
                "receipt_email" => $email,
                "shipping" => [
                    "name" => $name,
                    "address" => [
                        "line1" => $adresse,
                        "postal_code" => $code_postal,
                        "city" => $ville
                    ],
                    "phone" => $telephone,
                    "carrier" => null,
                    "tracking_number" => null
                ]
            ]);
            $status["state"] = "OK";
            return [$status, $charge];
        } catch (\Exception $exception) {
            switch ($exception->getMessage()) {
                case 'Your card number is incorrect.':
                    $message = 'Votre numéro de carte est incorrect';
                    break;
                case 'Your card has insufficient funds.':
                    $message = 'Votre carte à été refuser par votre banque pour <strong>fond insuffisant</strong>';
                    break;
                case 'Your card has expired.':
                    $message = 'Votre carte est expiré';
                    break;
                case 'Your card\'s security code is incorrect.':
                    $message = 'Le code de sécurité de votre carte est incorrect';
                    break;
                case 'An error occurred while processing your card. Try again in a little bit.':
                    $message = 'Une erreur s\'est produite lors du traitement de votre carte. Veuillez réesayer plus tard.';
                    break;
                case 'Your card was declined.':
                    $message = "Votre carte à été refuser";
                    break;
                default:
                    $message = 'Erreur Inconnue';
            }
            $status['state'] = "ERROR";
            Session::flash("error", $message);
            return [$status];
        }
    }

    public function addCheckout($cart_id, $user_id, $status)
    {
        $checkout = $this->checkoutRepository->create(
            $cart_id,
            $user_id,
            $status
        );
        return $checkout;
    }

    public function addCheckoutProducts(array $products, $checkout_id)
    {
        foreach ($products as $product) {
            $this->checkoutProductRepository->insert(
                $checkout_id,
                $product['id'],
                $product["qte"],
                $product["amount"]
            );
        }
    }

    public function addCheckoutPaiement($checkout_id, $mode_id, $total_paiement)
    {
        $this->checkoutPaiementRepository->create(
            $checkout_id,
            $mode_id,
            $total_paiement
        );
    }

    public function addCheckoutShippement($checkout_id, $shipping_id, $pds, $ship_amount, $suivi = 0)
    {
        $this->checkoutShippingRepository->create(
            $checkout_id,
            $shipping_id,
            $pds,
            $ship_amount,
            $suivi
        );
    }
}