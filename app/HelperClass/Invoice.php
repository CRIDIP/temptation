<?php


namespace App\HelperClass;


use App\Repository\Invoice\InvoicePaiementRepository;
use App\Repository\Invoice\InvoiceProductRepository;
use App\Repository\Invoice\InvoiceRepository;

class Invoice
{
    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    /**
     * @var InvoiceProductRepository
     */
    private $invoiceProductRepository;
    /**
     * @var InvoicePaiementRepository
     */
    private $invoicePaiementRepository;

    /**
     * Invoice constructor.
     * @param InvoiceRepository $invoiceRepository
     * @param InvoiceProductRepository $invoiceProductRepository
     * @param InvoicePaiementRepository $invoicePaiementRepository
     */
    public function __construct(
        InvoiceRepository $invoiceRepository,
        InvoiceProductRepository $invoiceProductRepository,
        InvoicePaiementRepository $invoicePaiementRepository
    )
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceProductRepository = $invoiceProductRepository;
        $this->invoicePaiementRepository = $invoicePaiementRepository;
    }

    public function addInvoice($id, $user_id, $totalOrder, $stateOrder)
    {
        $invoice = $this->invoiceRepository->create($id, $user_id, $totalOrder, $stateOrder);
        return $invoice;
    }

    public function addInvoiceProduct($invoice_id, $product_id, $qte, $total)
    {
        $product = $this->invoiceProductRepository->create($invoice_id, $product_id, $qte, $total);
        return $product;
    }

    public function addInvoicePaiement($invoice_id, $modePaiement, $montantPaiement)
    {
        $paiement = $this->invoicePaiementRepository->create($invoice_id, $modePaiement, $montantPaiement);
        return $paiement;
    }
}