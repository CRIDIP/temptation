<?php


namespace App\HelperClass;


use App\Repository\Order\OrderPaiementRepository;
use App\Repository\Order\OrderProductRepository;
use App\Repository\Order\OrderRepository;
use App\Repository\Order\OrderShippingRepository;
use App\Repository\Order\OrderStatusRepository;
use Illuminate\Support\Facades\Log;

class Order
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var OrderProductRepository
     */
    private $orderProductRepository;
    /**
     * @var OrderPaiementRepository
     */
    private $orderPaiementRepository;
    /**
     * @var OrderStatusRepository
     */
    private $orderStatusRepository;
    /**
     * @var OrderShippingRepository
     */
    private $orderShippingRepository;

    /**
     * Order constructor.
     * @param OrderRepository $orderRepository
     * @param OrderProductRepository $orderProductRepository
     * @param OrderPaiementRepository $orderPaiementRepository
     * @param OrderStatusRepository $orderStatusRepository
     * @param OrderShippingRepository $orderShippingRepository
     */
    public function __construct(OrderRepository $orderRepository,
                                OrderProductRepository $orderProductRepository,
                                OrderPaiementRepository $orderPaiementRepository,
                                OrderStatusRepository $orderStatusRepository,
                                OrderShippingRepository $orderShippingRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->orderProductRepository = $orderProductRepository;
        $this->orderPaiementRepository = $orderPaiementRepository;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->orderShippingRepository = $orderShippingRepository;
    }

    public function addOrder($customer_id, $total, $status, $cart_id)
    {
        $order = $this->orderRepository->create(
            $customer_id,
            $total,
            $status,
            $cart_id
        );
        return $order;
    }

    public function addOrderProduct($order_id, array $products)
    {
        foreach ($products as $product) {
            $this->orderProductRepository->insert(
                $order_id,
                $product["id"],
                $product["qte"],
                $product["amount"]
            );
        }
    }

    public function addOrderPaiement($order_id, $modePaiement, $numPaiement, $montantPaiement)
    {
        $paiement = $this->orderPaiementRepository->create(
            $order_id,
            $modePaiement,
            $numPaiement,
            $montantPaiement
        );

        return $paiement;
    }

    public function addOrderStatus($order_id, $state)
    {
        $status = $this->orderStatusRepository->create($order_id, $state);
        return $status;
    }

    public function addOrderShippement($order_id, $pds, $amount)
    {
        $ship = $this->orderShippingRepository->create(
            $order_id,
            $pds,
            $amount
        );
        return $ship;
    }

    public static function GetStateOrder($state)
    {
        switch ($state) {
            case 1:
                return "<span class='kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-times-circle'></i> Annulée</span>";
                break;
            case 2:
                return "<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-clock-o'></i> En attente du paiement à la livraison</span>";
                break;
            case 3:
                return "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-truck'></i> En attente de réapprovisionnement (Non Payé)</span>";
                break;
            case 4:
                return "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-truck'></i> En attente de réapprovisionnement (Payé)</span>";
                break;
            case 5:
                return "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-bank'></i> En attente de virement bancaire</span>";
                break;
            case 6:
                return "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-bank'></i> En attente de paiement par chèque</span>";
                break;
            case 7:
                return "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-dropbox'></i> En cours de préparation</span>";
                break;
            case 8:
                return "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-credit-card'></i> Erreur de paiement</span>";
                break;
            case 9:
                return "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-truck'></i> Expédié</span>";
                break;
            case 10:
                return "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-check'></i> Livré</span>";
                break;
            case 11:
                return "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-check'></i> Paiement à distance accepté</span>";
                break;
            case 12:
                return "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-credit-card'></i> Paiement accepté</span>";
                break;
            case 13:
                return "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-exchange'></i> Remboursé</span>";
                break;
            default:
                return "<span class='kt-badge kt-badge--dark kt-badge--inline kt-badge--pill kt-badge--lg'><i class='la la-warning'></i> Information Inconnue</span>";
                break;
        }
    }
}