<?php
if (!function_exists('currentRoute')) {
    function currentRoute(...$routes)
    {
        foreach($routes as $route) {
            if(request()->url() == $route) {
                return ' kt-menu__item--active';
            }
        }
    }
}

if (!function_exists('currentRouteAccount')) {
    function currentRouteAccount(...$routes)
    {
        foreach($routes as $route) {
            if(request()->url() == $route) {
                return 'kt-menu__item--here';
            }
        }
    }
}

if (!function_exists('currentRouteAdmin')) {
    function currentRouteAdmin(...$routes)
    {
        foreach($routes as $route) {
            if(request()->url() == $route) {
                return 'kt-menu__item--here';
            }
        }
    }
}
