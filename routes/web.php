<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);
Route::get('/contact', ["as" => "contact", "uses" => "ContactController@contact"]);
Route::post('/contact', ["as" => "contactPost", "uses" => "ContactController@contactPost"]);


Route::group(["prefix" => "category"], function (){
    Route::get('/{categorie_id}', ["as" => "categorie", "uses" => "HomeController@categorie"]);
    Route::get('/{categorie_id}/subs/{subs_id}', ["as" => "subcategorie", "uses" => "HomeController@subcategorie"]);
    Route::get('/{categorie_id}/{product_id}', ["as" => "product", "uses" => "HomeController@product"]);
});

Route::group(["prefix" => "cart", "namespace" => "Cart"], function (){
    Route::get('/', ["as" => "Cart.index", "uses" => "CartController@index"]);
    Route::post('createCart', ["as" => "Cart.createCart", "uses" => "CartController@createCart"]);
    Route::post('addProduct', ["as" => "Cart.addProduct", "uses" => "CartController@addProduct"]);
    Route::post('addWhitelist', ["as" => "Cart.addWhitelist", "uses" => "CartController@addWhitelist"]);
    Route::get('/deleteProduct/{product_id}', ["as" => "Cart.deleteProduct", "uses" => "CartController@deleteProduct"]);

    Route::get('/updateProduct', "CartController@updateProduct");
});

Route::group(["prefix" => "checkout", "namespace" => "Checkout"], function (){
    Route::get('/', ["as" => "Checkout.index", "uses" => "CheckoutController@index"]);
    Route::post('/', ["as" => "Checkout.checkout", "uses" => "CheckoutController@checkout"]);
});

Route::group(["prefix" => "backoffice", "namespace" => "Back", "middleware" => ['auth']], function (){
    Route::get('/', ["as" => "Back.dashboard", "uses" => "BackController@dashboard"]);

    Route::group(["prefix" => "sale", "namespace" => "Sale"], function (){
        Route::group(["prefix" => "order", "namespace" => "Order"], function (){
            Route::get('/', ["as" => "Back.Order.index", "uses" => "OrderController@index"]);
            Route::get('{orderId}', ["as" => "Back.Order.show", "uses" => "OrderController@show"]);

            Route::put('{orderId}/upState', ["as" => "Back.Order.upState", "uses" => "OrderController@upState"]);
            Route::post('{orderId}/generateInvoice', ["as" => "Back.Order.generateInvoice", "uses" => "OrderController@generateInvoice"]);

            Route::get('orderMonth', "OrderController@orderMonth");
            Route::get('listOrders', "OrderController@listOrders");
        });

        Route::group(["prefix" => "invoice", "namespace" => "Invoice"], function (){
            Route::get('/', ["as" => "Back.Invoice.index", "uses" => "InvoiceController@index"]);
        });

        Route::group(["prefix" => "credit", "namespace" => "Credit"], function (){
            Route::get('/', ["as" => "Back.Credit.index", "uses" => "CreditController@index"]);
        });

        Route::group(["prefix" => "delivery", "namespace" => "Delivery"], function (){
            Route::get('/', ["as" => "Back.Delivery.index", "uses" => "DeliveryController@index"]);
        });

        Route::group(["prefix" => "basket", "namespace" => "Basket"], function (){
            Route::get('/', ["as" => "Back.Basket.index", "uses" => "BasketController@index"]);
        });
    });

    Route::group(["prefix" => "catalog", "namespace" => "Catalog"], function (){
        Route::group(["prefix" => "product", "namespace" => "Product"], function (){
            Route::get('/', ["as" => "Back.Product.index", "uses" => "ProductController@index"]);
        });

        Route::group(["prefix" => "category", "namespace" => "Category"], function (){
            Route::get('/', ["as" => "Back.Category.index", "uses" => "CategoryController@index"]);
        });

        Route::group(["prefix" => "attribute", "namespace" => "Attribute"], function (){
            Route::get('/', ["as" => "Back.Attribute.index", "uses" => "AttributeController@index"]);

            Route::group(["prefix" => "feature", "namespace" => "Feature"], function (){
                Route::get('/', ["as" => "Back.Feature.index", "uses" => "FeatureController@index"]);
            });
        });
    });

    Route::group(["prefix" => "customer", "namespace" => "Customer"], function (){
        Route::get('/', ["as" => "Back.Customer.index", "uses" => "CustomerController@index"]);

    });

    Route::group(["prefix" => "sav", "namespace" => "Sav"], function (){
        Route::get('/', ["as" => "Back.Sav.index", "uses" => "SavController@index"]);

        Route::group(["prefix" => "return", "namespace" => "Return"], function (){
            Route::get('/', ["as" => "Back.Return.index", "uses" => "ReturnController@index"]);
        });
    });

    Route::group(["prefix" => "statistic", "namespace" => "Statistic"], function (){
        Route::get('/', ["as" => "Back.Statistic.index", "uses" => "StatisticController@index"]);
    });
});

Route::get('/code', "TestController@code");
Route::get('/construct', ["as" => "construct", "uses" => "HomeController@construct"]);
Route::get('policy', ["as" => "policy", "uses" => "HomeController@policy"]);
Route::get('cvg', ["as" => "cvg", "uses" => "HomeController@cvg"]);

Auth::routes();

Route::get('logout', ["as" => "logout", "uses" => "Auth\LoginController@logout"]);

